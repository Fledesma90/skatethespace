﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
//usamos esta variable para reiniciar el juego
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    #region ######## VARIABLES
    //distancia de caida a la linea de juego Game Over
    public float lowerLimit = -20f;

    [Header("Cheat Menu")]

    //referencia al panel con el menu de trucos
    public GameObject cheatMenu;

    public static GameManager instance;

    public float distanceScore;

    #endregion

    #region ######## EVENTS
    private void Awake() {
        if (instance == null) {
            instance = this;
        }
        //hacemos que no se destruya cuando se carga otra escena
        DontDestroyOnLoad(this.gameObject);
    }
    #endregion
    // Start is called before the first frame update
    void Start()
    {
        CreatedDefaultScores();
    }

    // Update is called once per frame
    void Update()
    {
        //si se mantiene pulsada la tecla ctrl y se pulsa una sola vez la Q alternamos la apertura del menú de trucos.
        if (Input.GetKeyDown(KeyCode.Q) && Input.GetKey(KeyCode.LeftControl)) {
            //toggle del menú de trucos
            cheatMenu.SetActive(!cheatMenu.activeSelf);
        }
    }
    #region ######### METHODS

    /// <summary>
    /// Proceso de fin de partida
    /// </summary>
    public void GameOver() {
        distanceScore = Mathf.Round(PlayerController.instance.meters);
        GoScoreScene();
    }
    /// <summary>
    /// Reiniciamos el nivel
    /// </summary>
    public void RestartLevel() {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }
    /// <summary>
    /// Transferimos información a la escena de Score Board
    /// </summary>
    public void GoScoreScene(){
        SceneManager.LoadScene("ScoreBoard");
    }

    /// <summary>
    /// Comprobamos si no existen puntuaciones 1,2,3 y nombres 1,2,3.
    /// Ponemos uno por defecto.
    /// </summary>
    public void CreatedDefaultScores() {
        if (!PlayerPrefs.HasKey("HighScoreName1")) {
            PlayerPrefs.SetString("HighScoreName1", "AAA");
        }
        if (!PlayerPrefs.HasKey("HighScoreName2")) {
            PlayerPrefs.SetString("HighScoreName2", "AAA");
        }
        if (!PlayerPrefs.HasKey("HighScoreName3")) {
            PlayerPrefs.SetString("HighScoreName3", "AAA");
        }


        if (!PlayerPrefs.HasKey("HighScore1")) {
            PlayerPrefs.SetFloat("HighScore1", 0f);
        }
        if (!PlayerPrefs.HasKey("HighScore2")) {
            PlayerPrefs.SetFloat("HighScore2", 0f);
        }
        if (!PlayerPrefs.HasKey("HighScore3")) {
            PlayerPrefs.SetFloat("HighScore3", 0f);
        }
    }

    /// <summary>
    /// Metodo que fija el timescale con el valor recibido como parametro
    /// </summary>
    /// <param name="time"></param>
    public void SetTimeScale(float time) {
        Time.timeScale = time;

    }
    #endregion 
}
