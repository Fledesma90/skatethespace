﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class ScoreManager : MonoBehaviour
{
    //variable para guardar la distancia que se ha recorrido
    float score;
    //Referencia al inputfield
    //De el inputfield cogeremos el texto que se haya escrito y lo asignaremos como nombre
    public TMP_InputField inputText;
    public TextMeshProUGUI yourScoreText;

    public TextMeshProUGUI highScore1;
    public TextMeshProUGUI highScore2;
    public TextMeshProUGUI highScore3;

    // Start is called before the first frame update
    void Start()
    {
        score = GameManager.instance.distanceScore;
        yourScoreText.text = score.ToString();
        UpdateScoreBoard();
    }
    /// <summary>
    /// Creamos una funcion publica para guardar el score al pulsar el botón save score.
    /// Comprobamos en qué posicion va esta nueva puntuación y cambiamos el valor en el PlayerPrefs
    /// creamos variables temporales para guardar los antiguos valores y que se ejecute
    /// el desplazamiento adecuadamente.
    /// </summary>
    public void StoreNameAndScore()
    {
        
        string name = inputText.text;

        if (score> PlayerPrefs.GetFloat("HighScore1"))
        {
            string oldHS1Name = PlayerPrefs.GetString("HighScoreName1");
            float oldHS1Score  = PlayerPrefs.GetFloat("HighScore1");


            string oldHS2Name = PlayerPrefs.GetString("HighScoreName2");
            float oldHS2Score = PlayerPrefs.GetFloat("HighScore2");

            PlayerPrefs.SetString("HighScoreName1", name);
            PlayerPrefs.SetFloat("HighScore1", score);

            PlayerPrefs.SetString("HighScoreName2", oldHS1Name);
            PlayerPrefs.SetFloat("HighScore2", oldHS1Score);

            PlayerPrefs.SetString("HighScoreName3", oldHS2Name);
            PlayerPrefs.SetFloat("HighScore3", oldHS2Score);
        }
        else if(score < PlayerPrefs.GetFloat("HighScore1") && score > PlayerPrefs.GetFloat("HighScore2"))
        {
            string oldHS2Name = PlayerPrefs.GetString("HighScoreName2");
            float oldHS2Score = PlayerPrefs.GetFloat("HighScore2");

            PlayerPrefs.SetString("HighScoreName2", name);
            PlayerPrefs.SetFloat("HighScore2", score);

            PlayerPrefs.SetString("HighScoreName3", oldHS2Name);
            PlayerPrefs.SetFloat("HighScore3", oldHS2Score);
        }
        else if (score < PlayerPrefs.GetFloat("HighScore2") && score > PlayerPrefs.GetFloat("HighScore3"))
        {
            PlayerPrefs.SetString("HighScoreName3", name);
            PlayerPrefs.SetFloat("HighScore3", score);
        }
        else
        {
            Debug.Log("Tu puntuacion no entra en el ranking");
        }
        UpdateScoreBoard();
    }
    /// <summary>
    /// Vamos reponiendo los nombres dependiendo de los valores o puntuación obtenida.
    /// </summary>
    void UpdateScoreBoard()
    {
        highScore1.text = PlayerPrefs.GetString("HighScoreName1") + " - " +PlayerPrefs.GetFloat("HighScore1");
        highScore2.text = PlayerPrefs.GetString("HighScoreName2") + " - " +PlayerPrefs.GetFloat("HighScore2");
        highScore3.text = PlayerPrefs.GetString("HighScoreName3") + " - " +PlayerPrefs.GetFloat("HighScore3");
    }

    // Update is called once per frame
    void Update()
    {

    }
}
