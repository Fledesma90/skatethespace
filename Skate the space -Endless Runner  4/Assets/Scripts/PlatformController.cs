﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlatformController : MonoBehaviour
{
    #region ################## VARIABLES
    public float platformSize=10f;
    public float destroyMargin = 30f;
    private Camera cam;
    #endregion

    #region ################## EVENTS
    
    // Start is called before the first frame update
    void Start()
    {
        //lo que hace es buscar la camara en la escena y rellena la variable cam con
        //este dato
        cam = Camera.main;
    }

    // Update is called once per frame
    void Update()
    {
        CheckDestroyMargin();
    }
    #endregion

    #region ############## METHODS

    //Creamos variable para destruir plataformas
    void DestroyPlatform() { 
        PlatformManager.instance.Spawn();

        Destroy(gameObject);
    }
    void CheckDestroyMargin() {
        if(transform.position.x<(cam.transform.position.x - destroyMargin)) {
            DestroyPlatform();
        }
        //creamos una linea verde para representar el margen de destrucción de plataforma llamando
        //a la función creada a priori
        Vector3 lineA = new Vector3(cam.transform.position.x - destroyMargin,
                                    cam.transform.position.y + 10f,
                                    0);
        Vector3 lineB = new Vector3(cam.transform.position.x - destroyMargin,
                                  cam.transform.position.y - 10f, 0);
        Debug.DrawLine(lineA, lineB, Color.green);
    }

    #endregion
}
