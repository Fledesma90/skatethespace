﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LowerLimit : MonoBehaviour
{
    #region #####VARIABLES
    private Camera mainCamera;
    #endregion

    #region ######EVENTS
    // Start is called before the first frame update
    void Start()
    {
        mainCamera = Camera.main;
    }

    // Update is called once per frame
    void Update()
    {
        transform.position = new Vector3(mainCamera.transform.position.x,
                                        GameManager.instance.lowerLimit,
                                        0f);
    }
    #endregion
}
