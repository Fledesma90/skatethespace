﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class CameraBehaviour : MonoBehaviour
{
    public Transform playerTarget;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        //Anclamos la camara al player
        transform.position = new Vector3(playerTarget.position.x, transform.position.y,
                                       transform.position.z);
    }

}
