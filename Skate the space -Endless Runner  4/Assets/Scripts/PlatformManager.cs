﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlatformManager : MonoBehaviour
{
    #region ################## VARIABLES
    //numero inicial de plataformas generadas
    public int initialPlatforms = 10;
    //prefab de la plataforma a construir
    public GameObject[] platformPrefabs;
    public GameObject[] platformPrefabs1;
    public GameObject[] platformPrefabs2;
    public GameObject[] platformPrefabs3;
    //margenes entre plataformas
    public float horizontalMin = 1f;
    public float horizontalMax = 5f;
    //margenes verticales entre plataformas
    //FELIX PROBABLEMENTE TENGAS QUE QUITAR LOS VERTICALS PORQUE NO TE HACEN FALTA YA QUE VAS A PONER PLATAFORMAS
    //DEL MISMO NIVEL
    //le ponemos la misma distancia porque no tenemos pensado cambiar la posicion de las plataformas
    //en el eje y, pero por si acaso las dejamos por si más adelante queremos hacerlo.
    public float verticalMin = -4f;
    public float verticalMax = -4f;
    //referencia al transform que será el padre de las plataformas
    public Transform platformContainer;
    //Posicion de la generacion de la siguiente plataforma
    private Vector2 nextPosition;

    //con esto lo que hacemos es que este script sea mas accesible en otros objetos.
    public static PlatformManager instance;

    public int startPlatformCounter;
    private int platformCounter;
    public GameObject[] powerUpPrefabs;
    
    #endregion

    #region ####################### EVENTS

    private void Awake() {
        //instance en este caso representa PlatformManager
        if (instance == null) {
            instance = this;
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        // tomo como siguiente posicion de generción, la posicion del propio PlatformManager
        nextPosition = transform.position;

        for (int i = 0; i < initialPlatforms; i++) {
            Spawn();
        }
        platformCounter = startPlatformCounter;
    }

    // Update is called once per frame
    void Update()
    {
        
    }


    #endregion

    #region #################### METHODS
    /// <summary>
    /// Genera una nueva plataforma, en la  posicion aleatoria dentro de los margenes definidos
	/// y dependiendo de la distancia recorrida elegimos las plataformas de un array u otro
	/// que contiene unos enemigos u otros.
    /// </summary>
    public void Spawn() {
        
        GameObject[] platforms;
        if (PlayerController.instance.meters < 50)
        {
            platforms=platformPrefabs;
        }else if (PlayerController.instance.meters < 100 && PlayerController.instance.meters>50)
        {
            platforms = platformPrefabs1;
        }else if (PlayerController.instance.meters < 150 && PlayerController.instance.meters>100)
        {
            platforms = platformPrefabs2;
        }
        else
        {
            platforms = platformPrefabs3;
        }
        //Decidimos de forma aleatoria, cual será la desviacion en la siguiente posición.
        Vector2 randomPosition = nextPosition + new Vector2(Random.Range(horizontalMin, horizontalMax),
                                                        Random.Range(verticalMin, verticalMax));
        //Establecemos la generación de plataformas en y a partir de -4 que es la posicion de la plataforma
       if (randomPosition.y < -4) {
            randomPosition = new Vector2(randomPosition.x, -4);
        }
        //si la plataforma se iba a generar por debajo del limite inferior en su lugar, la ajusto a la altura de ese límite.
        /*if (randomPosition.y < GameManager.instance.lowerLimit) {
            randomPosition.y = GameManager.instance.lowerLimit;
        }*/
        //elegimos con un random qué plataforma vamos a instanciar
        int randomPlatform = Random.Range(0, platforms.Length);

        GameObject tempPlatform = Instantiate(platforms[randomPlatform], randomPosition, Quaternion.identity, platformContainer);

        //tamaño de la plataforma generada recuperado desde el tamaño de la plataforma. De esta forma da igual el tamaño que tenga
        //mientras esta bien definido yo podre crear plataformas del tamaño que necesite
        float temPlatformSize = tempPlatform.GetComponent<PlatformController>().platformSize;

        randomPosition.x += 15;

        nextPosition = randomPosition;
        //creamos una variable platformCounter para contar las plataformas en las que se va a
        //crear o instanciar un PowerUp. Con la variable platformCounter le quitamos uno al contador
        //de plataformas para que se pongan powerUps.
        platformCounter--;
        if (platformCounter <= 0) {
            SpawnPowerUp(tempPlatform.transform.position);
        }
    }
    //Creamos función para instanciar Power Ups en posiciones aleatorias
    //durante el transcurso del juego
    void SpawnPowerUp(Vector2 PlatformPosition) {
        Vector2 SpawnPosition = new Vector2(PlatformPosition.x+Random.Range(-3,3), PlatformPosition.y+Random.Range(3,6));
        Instantiate(powerUpPrefabs[Random.Range(0,powerUpPrefabs.Length)], SpawnPosition, Quaternion.identity);
        platformCounter = startPlatformCounter;
            
    }
    #endregion
}
