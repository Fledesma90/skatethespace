﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//forzamos a que exista un componente AudioSource
[RequireComponent(typeof(AudioSource))]
public class SoundManager : MonoBehaviour
{
    #region ######VARIABLES
    private AudioSource audioSource;

    public static SoundManager instance;
    #endregion

    #region #######EVENTS

    private void Awake() {
        if (instance == null) {
            instance = this;
        }
    }
    // Start is called before the first frame update
    void Start()
    {
        audioSource=GetComponent<AudioSource>();
    }
 
    #endregion

    #region #######METHODS

    /// <summary>
    /// Reproduce el audioclip recibido como parámetro
    /// </summary>
    ///<param name="clip"></param>
    public void PlayClip(AudioClip clip) {
        audioSource.PlayOneShot(clip);
    }
   
    #endregion
}