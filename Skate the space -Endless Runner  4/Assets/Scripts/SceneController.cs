﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
//para cambios de escena
using UnityEngine.SceneManagement;

public class SceneController : MonoBehaviour {

    #region METHODS
    /// <summary>
    /// Cambia a la escena recibida como parámetro
    /// </summary>
    /// <param name="sceneName"></param>
    public void ChangeScene(string sceneName) {
        //reiniciamos escala de tiempo
        Time.timeScale = 1f;
        //cargamos la escena
        SceneManager.LoadScene(sceneName);
    }
    public void ExitGame() {
        //cierra la aplicacion (esto solo funciona con la build)
        Application.Quit();
    }
    #endregion
}