﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Parallax : MonoBehaviour {
    #region 
    //atenuacion del movimiento horizontal, cuanto menor sea el valor, menos se va a mover
    public float attenX = 0.01f;
    //atenuacion del movimiento vertical
    public float attenY = 0.02f;
    //posicion para control del offset de la textura
    private Vector2 pos = Vector2.zero;
    //referencia al main camera
    private Transform cam;
    //posicion anterior de la camara
    private Vector2 cam0ldPos;
    //referencia al renderer del background
    private Renderer rend;
    #endregion

    #region EVENTS
    // Start is called before the first frame update
    void Start() {
        //recuperamos la referencia a la primera camara con el tag MainCamera
        cam = Camera.main.transform;
        //inicializamos la posicion de la cámara
        cam0ldPos = cam.position;
        // recuperamos la referencia al componente Renderer
        rend = GetComponent<Renderer>();
    }

    // Update is called once per frame
    void Update() {
        Vector2 camVar = new Vector2(cam.position.x - cam0ldPos.x,
                                     cam.position.y - cam0ldPos.y);
        //modificamos el offset que se aplicará a la textura
        //aplicando la atenuacion de movimiento indicada como parametro
        pos.Set(pos.x + (camVar.x * attenX),
               0f);
        //en el caso del movimiento verrtical, desplazamos todo el quad, para evitar
        //las repeticiones verticales de la textura
        transform.position = new Vector3(transform.position.x,
                                                  transform.position.y + (camVar.y * attenY),
                                                  transform.position.z);

        //aplicamos el offset a la textura principal
        rend.material.SetTextureOffset("_MainTex", pos);
        //actualizamos la posicion de la camara para el siguiente ciclo
        cam0ldPos = cam.position;
    }
    #endregion
}
