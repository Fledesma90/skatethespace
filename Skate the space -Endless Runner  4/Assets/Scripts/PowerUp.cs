﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PowerUp : MonoBehaviour
{
    #region ######## VARIABLES
    //sonido que se reproduce cuando se recoge el PowerUp
    public AudioClip sound;
    //Creamos un enum para elegir el tipo de PowerUp
    public enum PowerUps { shoot,lives,doublejump}
    //Variable para llamar en el Prefab de PowerUp el tipo de PowerUp
    public PowerUps powerUpType;
    
    public static PowerUp instance1;
    //Efecto creado para tipo de PowerUp
    public GameObject effectShoot;
    //Efecto creado para tipo de PowerUp
    public GameObject effectLife;
    //Efecto creado para tipo de PowerUp
    public GameObject effectJump;
    #endregion
    #region EVENTS
    /// <summary>
    /// Con esta función establecemos que por cada PowerUp se le atribuirá
    /// al personaje una habilidad
    /// </summary>
    /// <param name="collision"></param>
    private void OnTriggerEnter2D(Collider2D collision) {
        if (collision.CompareTag("Player")) {
            if (powerUpType == PowerUps.shoot) {
                collision.GetComponent<PlayerController>().canShoot = true;
                Instantiate(effectShoot, transform.position, Quaternion.identity);
            }
            if (powerUpType == PowerUps.lives) {
                collision.GetComponent<PlayerController>().AddLives();
                Instantiate(effectLife, transform.position, Quaternion.identity);

            }
            if (powerUpType == PowerUps.doublejump)
            {
                collision.GetComponent<PlayerController>().DoubleJump();
                Instantiate(effectJump, transform.position, Quaternion.identity);


            }
            collision.GetComponent<PlayerController>().CallPowerUpTime();
            SoundManager.instance.PlayClip(sound);
            Destroy(gameObject);
        }
    }


    #endregion
}
