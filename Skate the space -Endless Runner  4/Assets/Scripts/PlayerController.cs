﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class PlayerController : MonoBehaviour
{
    #region ############### VARIABLES

    [Header("Jump Settings")]
    public float jumpForce = 7f;

    [Header("Ground Check")]

    public Transform groundCheck;

    public LayerMask groundLayer;

    public Vector2 sizeGroundCheck = new Vector2(0.64f, 0.09f);

    public bool grounded = false;

    [Header("Movement")]
    //indica que se movera de forma continua
    public bool autoMove = true;
    //velocidad maxima permitida
    public float maxSpeed = 5f;
    //aceleracion aplicada
    public float acceleration = 20f;

    //variable que almacenará la referencia al RigidBody2D
    private Rigidbody2D rigidbody;

    public float meters = 0f;

    private Vector2 startPosition;

    public TextMeshProUGUI metersText;

    public float gravityNormal;

    public float gravityHolding;

    public bool canShoot;

    public GameObject bulletPrefab;

    public Transform ShootPoint;

    public float bulletSpeed;

    public AudioClip shootSound;
    public AudioClip jumpSound;
    public int JumpsInAirCurrent = 0;
    public int jumpsInAirAllowed = 1;
    public int lives;
    public GameObject lastPlatform;
    public TextMeshProUGUI livesText;
    public static PlayerController instance;
    //Variable que hace referencia a la animator
    public Animator anim;
    #endregion

    #region ################ EVENTS

    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        startPosition = transform.position;

        rigidbody = GetComponent<Rigidbody2D>();

    }

    // Update is called once per frame
    void Update()
    {
        EvaluatePhysics();
        controls();
        CountDistance();
        Movement();
        DeadZoneCheck();
    }

    private void LateUpdate() {
        Movement();
    }

    private void OnDrawGizmos() {
        Gizmos.color = Color.red;
        Gizmos.DrawWireCube(groundCheck.position, sizeGroundCheck);
    }

    #endregion

    #region ############### METHODS

    /// <summary>
    /// Comprobaciones de fisicas y contacto con suelo
    /// </summary>
    void EvaluatePhysics() {
        grounded = Physics2D.OverlapBox(groundCheck.position, sizeGroundCheck,0f,groundLayer);
        if (Physics2D.OverlapBox(groundCheck.position, sizeGroundCheck, 0f, groundLayer) != null) {
            lastPlatform = Physics2D.OverlapBox(groundCheck.position, sizeGroundCheck, 0f, groundLayer).gameObject;

        }
        if (grounded) {
            JumpsInAirCurrent = 0;
            anim.SetBool("Jumping", false);
        } else {
            anim.SetBool("Jumping", true);
        }
        if (rigidbody.velocity.y > 0) {
            anim.SetBool("GoingUp", true);
        }
        if (rigidbody.velocity.y < 0) {
            anim.SetBool("GoingUp", false);
        }
    }

    void UpdateLivesUI() {
        livesText.text = lives.ToString();
    }
    /// <summary>
    /// Evaluamos la interaccion con los controles
    /// </summary>
    void controls() {
        //si se ha detectado la tecla de salto
        if (Input.GetButtonDown("Jump")) {
            //saltamos
            Jump();

        }

        if (Input.GetButton("Jump")&& rigidbody.velocity.y>0) {
            rigidbody.gravityScale = gravityHolding;
        } else {
            rigidbody.gravityScale = gravityNormal;
        }
        if(Input.GetButtonDown("Fire1")&& canShoot) {
            Shoot();
            SoundManager.instance.PlayClip(shootSound);


        }
        if (Input.GetKeyDown(KeyCode.A)) {
            //invierto el valor de la booleana automove. Hay informacion de esto en la web de unity directivas de defines de plataforma
            autoMove = !autoMove;
            rigidbody.velocity = Vector3.zero;
        }
    }
    /// <summary>
    /// Acción de salto
    /// </summary>
    public void Jump() {
        //si se está tocando el suelo
        if (grounded || JumpsInAirCurrent<jumpsInAirAllowed) {
            rigidbody.velocity = new Vector2(rigidbody.velocity.x, 0f);
            //aplicamos la fuerza de salto vertical hacia arriba y el tipo de impulso
            rigidbody.AddForce(Vector2.up * jumpForce, ForceMode2D.Impulse);
            SoundManager.instance.PlayClip(jumpSound);

            if (!grounded) {
                JumpsInAirCurrent++;
            }
        }
    }
    /// <summary>
    /// Aplica el movimiento
    /// </summary>
    public void Movement() {
        //si el automove esta desactivado, no hago nada.
        if (!autoMove || (!grounded && rigidbody.velocity.y<0)) {
            return;
        }
        //almaceno la velocidad actual del rigidbody
        Vector2 tempVelocity = rigidbody.velocity;

        //modifico el valor x (horizontal) para aproximarlo al valor de la velocidad deseada, mediante una
        //aceleracion progresiva.
        //habra que multiplicarlo por delta time, para que se aplique la aceleracion correspondiente a la
        //fraccion de tiempo transcurrida desde el ultimo frame.
        //De esta forma, la aceleracion sera igual independientemente de la tasa de frames por segundo
        tempVelocity.x = Mathf.MoveTowards(tempVelocity.x, maxSpeed, Time.deltaTime * acceleration);

        rigidbody.velocity = tempVelocity;
    }

    private void CountDistance() {
        meters = Vector2.Distance(startPosition, transform.position);
        metersText.text = meters.ToString("F0")+" meters";
    }

    void Shoot() {
        GameObject bulletInstance = Instantiate(bulletPrefab, ShootPoint.position, Quaternion.identity);
        bulletInstance.GetComponent<Rigidbody2D>().velocity = transform.right * bulletSpeed;
    }

    private void OnTriggerEnter2D(Collider2D collision) {
        //comprobamos si ha colisionado con un enemigo
        if (collision.CompareTag("Enemy")) {
            lives--;
            if (lives <= 0) {
                PlayerDead();
            } else {
                Destroy(collision.gameObject);
                MovePlayerLastPlatform();
            }

        }
    }
    public void CallPowerUpTime()
    {
        StartCoroutine(PowerUpTime());
        
    }

    IEnumerator PowerUpTime() {
        yield return new WaitForSeconds(7);
        canShoot = false;
        if (jumpsInAirAllowed > 0)
        {
            jumpsInAirAllowed--;
        }
    }
    /// <summary>
    /// comprueba si el jugador ha caido por debajo del limite mortal 
    /// </summary>
    private void DeadZoneCheck() {
        if (transform.position.y < GameManager.instance.lowerLimit) {
            lives--;
            if (lives <= 0) {
                GameManager.instance.GameOver();
                
            } else {
                //movemos el jugador a la última plataforma
                MovePlayerLastPlatform();
            }
        }
        
    }
    void    MovePlayerLastPlatform() {
        transform.position = new Vector3(lastPlatform.transform.position.x + 1, lastPlatform.transform.position.y + 4, transform.position.z);
        UpdateLivesUI();
        rigidbody.velocity = Vector3.zero;

    }
    public void PlayerDead() {
        //detengo el movimiento del jugador
        autoMove = false;

        //paramos la simulacion de fisicas
        rigidbody.simulated = false;
        //llamamos al metodo de fin de partida
        GameManager.instance.GameOver();
    }
    public void AddLives() {
        if (lives + 1 <= 3){
            lives++;
        }
        
        UpdateLivesUI();
        
    }

    public void DoubleJump()
    {
        jumpsInAirAllowed++;
        
    }

    /// <summary>
    /// Activa el powerup
    /// </summary>
    public void PowerUpOn() {
        //indico que se inicia el powerup
        canShoot= true;
        
    }

    /// <summary>
    /// Desactiva el powerUp
    /// </summary>
    public void PowerUpOff() {
        //indico que se desactiva el powerup
        canShoot = false;
        
    }

    public void DoubleJumpOff() {
        jumpsInAirAllowed = 1;

    }

    public void AddLivesOn() {
        lives = 99;

        UpdateLivesUI();
    }

    public void AddLivesOff() {
        lives=3;

        UpdateLivesUI();
    }
    #endregion
}
